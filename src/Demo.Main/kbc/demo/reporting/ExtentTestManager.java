package kbc.demo.reporting;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import java.util.HashMap;
import java.util.Map;

public class ExtentTestManager {
    static ExtentReports xReport = ExtentManager.createReportInstance();
    static ExtentTest xTest;
    static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();

    //START TEST REPORT
    public  static synchronized ExtentTest startTest(String testName) {
        xTest = xReport.createTest(testName);
        extentTestMap.put((int) (long) (Thread.currentThread().getId()), xTest);
        return xTest;
    }

    //END TEST REPORT
    public static synchronized void endTest() {
        xReport.flush();
    }

    //GET TEST REPORT
    public static synchronized ExtentTest getTest() {
        return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId()));
    }
}
