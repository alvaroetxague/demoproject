package kbc.demo.reporting;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentManager {
    //Declare
    private static ExtentReports xReport;
    private static ExtentHtmlReporter htmlReport;

    public static ExtentReports createReportInstance(){
        //EXAMPLE 1: Declare and define 'klov' reporter
        //ExtentKlovReporter klov = new ExtentKlovReporter("project", "build");

        //EXAMPLE 2: Declare and define 'html' reporter
        //ExtentEmailReporter: Builds an emailable HTML file
        //ExtentHtmlReporter (version-3): Builds a single HTML file with multiple views (SPA)
        //ExtentKlovReporter: Updates MongoDb upon invocation of each TestListener event
        //Initialize and define the path where output is to be printed
        htmlReport = new ExtentHtmlReporter("C:\\Users\\echaro\\IdeaProjects\\DemoProject\\" +
                "TestReport\\AlvaroAutomationReport.html");

        //Report Configuration
        htmlReport.config().setTheme(Theme.DARK);
        htmlReport.config().setDocumentTitle("AutomationTitle");
        htmlReport.config().setEncoding("utf-8");
        htmlReport.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss");
        htmlReport.config().setAutoCreateRelativePathMedia(true);
        htmlReport.config().setProtocol(Protocol.HTTPS);
        htmlReport.config().setReportName("Alvaro Demo Report");
        htmlReport.config().setJS("js-string");
        htmlReport.config().setCSS("css-string");
        htmlReport.config().setCSS(".r-img { width: 40%; }");

        //Create report instance
        xReport = new ExtentReports();
        //To enable or start a reporter, use attachReporter
        xReport.attachReporter(htmlReport);

        return xReport;
    }

    public static ExtentReports getInstance() {
        if (xReport == null)
            createReportInstance();
        return xReport;
    }
}
