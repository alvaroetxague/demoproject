package kbc.demo.base;

import kbc.demo.configuration.Constants;
import kbc.demo.reporting.ExtentManager;
import kbc.demo.reporting.ExtentTestManager;
import kbc.demo.utilities.GenericMethods;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePageObject {

    //WEBDRIVER AND WAITS
    protected WebDriver driver;
    protected WebDriverWait wait;

    //UTILITIES INITIALIZER
    protected GenericMethods gm;

    //REPORTERS
    protected ExtentManager xManager;
    protected ExtentTestManager xTestMan;

    //PAGES
    protected Constants constants;

    //LOGGER INITIALIZER
    protected static final Logger log = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());

    //CLASS CONSTRUCTOR
    protected BasePageObject(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }
}
