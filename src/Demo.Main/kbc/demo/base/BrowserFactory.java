package kbc.demo.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class BrowserFactory extends BasePageObject{

    //CLASS CONSTRUCTOR
    protected BrowserFactory(WebDriver driver) {
        super(driver);
    }

    public static WebDriver getDriver(String browser) {
        WebDriver driver = null;
        if ("Firefox".equals(browser)) {
            System.setProperty("webdriver.gecko.driver", "src\\Demo.Main\\resources\\geckodriver.exe");
            driver = new FirefoxDriver();
        } else if ("Chrome".equals(browser)) {
            System.setProperty("webdriver.chrome.driver", "src\\Demo.Main\\resources\\chromedriver.exe");
            driver = new ChromeDriver();
        } else {
            System.setProperty("webdriver.gecko.driver", "src\\Demo.Main\\resources\\geckodriver.exe");
            driver = new FirefoxDriver();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return driver;
    }

}
