package kbc.demo.utilities;

import org.apache.commons.mail.*;

public class SendEmail {

    public void sendSimpleEmail() throws EmailException {
        Email email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("alvaroetxague@gmail.com", "rAsputin23"));
        email.setSSLOnConnect(true);
        email.setFrom("alvaroetxague@gmail.com");
        email.setSubject("HVH - Alvaro Test Report");
        email.setMsg("This is a test mail ... :-)");
        email.addTo("echaro@amazon.com");
        email.send();
        System.out.println("==================Email Sent=================");
    }

    public void sendEmailWithAttachments() throws EmailException {
        // Create the attachment
        EmailAttachment reportAttachment = new EmailAttachment();

        //Report Attachment location and details
        reportAttachment.setPath("C:\\Users\\echaro\\IdeaProjects\\DemoProject\\TestReport\\" +
                "AlvaroAutomationReport.html");
        reportAttachment.setDisposition(EmailAttachment.ATTACHMENT);
        reportAttachment.setDescription("HVH - UI Automation Test Alvaro Echague");

        //Attachment name as it will appear in the email
        reportAttachment.setName("AlvaroAutomationReport.html");

        // Create the email message
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("alvaroetxague@gmail.com", "rAsputin23"));
        email.setSSLOnConnect(true);
        email.setFrom("alvaroetxague@gmail.com");
        email.setSubject("Automation Execution Report");
        email.setMsg("Hi All, \n\nPlease find attached the report from the latest automation run in UAT. \n\nDouble " +
                "click the attachment to open it in your browser.\n\nRegards," +
                "\nAlvaro");

        //List of users that will receive the email
        email.addTo("echaro@amazon.com", "Alvaro Echague");
//        email.addTo("hecgb@amazon.com", "Hector Barata");

        // add the attachment
        email.attach(reportAttachment);

        // send the email
        email.send();
        System.out.println("==================Email Sent=================");
    }
}