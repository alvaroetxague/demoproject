package kbc.demo.utilities;

import kbc.demo.base.BasePageObject;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Method;

public class GenericMethods extends BasePageObject {

    //CLASS CONSTRUCTOR
    public GenericMethods(WebDriver driver) {
        super(driver);
    }

    //TYPE TEXT IN A FIELD
    public void type(String text, WebElement element){
        element.sendKeys(text);
    }

    //CLICK ANYTHING
    public void click(WebElement element){
        element.click();
    }

    //WAIT FOR AN EXPECTED CONDITION TO HAPPEN WITHIN A 'X' AMOUNT OF SECONDS
    public void waitFor(ExpectedCondition<WebElement> condition, Integer timeOutInSeconds){
        timeOutInSeconds = timeOutInSeconds != null ? timeOutInSeconds : 20;
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(condition);
    }

    //WAIT FOR AN ELEMENT OT BE VISIBLE IN THE PAGE
    public void waitForVisibilityOf(WebElement element, Integer... timeOutInSeconds){
        int attempts = 0;
        while(attempts < 2){
            try{
                waitFor(ExpectedConditions.visibilityOf(element), (timeOutInSeconds.length > 0 ?
                        timeOutInSeconds[0] : null));
                break;
            }
            catch (StaleElementReferenceException e){
                log.error(e.getMessage());
            }
            attempts++;
        }
    }

    //RETURN TEXT FROM A WEB ELEMENT
    public String getText(WebElement element){
        String text = element.getText();
        System.out.println("Text is: " + text);
        return text;
    }

    //GO TO A URL
    public void goToUrl(String url){
        driver.get(url);
    }

    //GET PAGE TITLE
    public String getPageTitle(){
        String title = driver.getTitle();
        return title;
    }

    //CLOSE DOWN/QUIT THE BROWSER
    public void closeBrowser(){
        driver.quit();
    }

    //GET METHOD NAME
    public void getTestMethodName(Method method) {
        String testMethodName = method.getName(); //This will be:verifySaveButtonEnabled
//        String descriptiveTestName = method.getAnnotation(Test.class).testName(); //This will be: 'Verify if the save button is enabled'
//        test = extent.createTest(descriptiveTestName);
    }
}
