package kbc.demo.pages;

import kbc.demo.base.BasePageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Home extends BasePageObject {

    //CLASS CONSTRUCTOR
    public Home(WebDriver driver) {
        super(driver);
    }

    //DEFINE WEB ELEMENTS
    @FindBy(xpath = "//h2[contains(text(),'Check out our mortgage switcher offer today')]")
    private WebElement checkOutMortgageText;

    //GETTERS
    public WebElement getCheckOutMortgageText() {
        return checkOutMortgageText;
    }

    //PAGE ACTIONS
    public String getMortgageText(){
        String mortgageText = gm.getText(checkOutMortgageText);
        return mortgageText;
    }

    //TEST ACTIONS
}
