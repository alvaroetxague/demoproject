package kbc.demo.listeners;

import com.aventstack.extentreports.ExtentTest;
import kbc.demo.reporting.ExtentTestManager;
import kbc.demo.utilities.SendEmail;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.*;

public class CustomListener implements ITestListener, ISuiteListener, IClassListener{

    protected static final Logger log = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
    protected ExtentTestManager xTestMan;
    protected ExtentTest xTest;
    protected SendEmail emails;

    //testAuthor will define the person who writes and/or executes the tests.
    protected String testAuthor = "Alvaro Echague";

    //testCategory will group the tests under a category. ie: DemoClass tests, positive tests, etc.
    protected String testCategory = "DemoClass Tests";

    @Override
    public void onTestStart(ITestResult result) {
        // When test method starts
        System.out.println("TEST STARTED -> Test Name: " + result.getName());
        log.info("TEST STARTED -> Test Name: " + result.getName());

        testInit(result, testAuthor, testCategory);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        // If test method is successful
        System.out.println("TEST PASSED -> Test Name: " + result.getName());
        log.info("TEST PASSED -> Test Name: " + result.getName());
        System.out.println("--------------------------------------------");
        log.info("--------------------------------------------");
        xTest.pass("Passed inside listener");
        xTestMan.endTest();
    }

    @Override
    public void onTestFailure(ITestResult result) {
        // If test method is failed
        System.out.println("TEST FAILED -> Test Name: " + result.getName());
        log.error("TEST FAILED -> Test Name: " + result.getName());
        System.out.println("--------------------------------------------");
        log.info("--------------------------------------------");
        xTest.fail("Failed inside listener");
        xTestMan.endTest();
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        // If test method is Skipped
        System.out.println("TEST SKIPPED -> Test Name: " + result.getName());
        log.error("TEST SKIPPED -> Test Name: " + result.getName());
        System.out.println("--------------------------------------------");
        log.info("--------------------------------------------");
        xTest.skip("Skipped inside listener");
        xTestMan.endTest();
    }

    @Override
    public void onStart(ITestContext context) {
        // Before <test> tag of xml file
        System.out.println("STARTED: " + context.getName());
        log.info("STARTED: " + context.getName());
        System.out.println("--------------------------------------------");
        log.info("--------------------------------------------");

        ITestNGMethod methods[] = context.getAllTestMethods();

        System.out.println("These methods will be executed in this test tag:");
        log.info("These methods will be executed in this test tag:");

        for (ITestNGMethod method : methods) {
            System.out.println(method.getMethodName());
            log.info(method.getMethodName());
        }
        System.out.println("--------------------------------------------");
        log.info("--------------------------------------------");
    }

    @Override
    public void onFinish(ITestContext context) {
        // After <test> tag of xml file
        System.out.println("COMPLETED: " + context.getName());
        log.info("COMPLETED: " + context.getName());
        System.out.println("--------------------------------------------");
        log.info("--------------------------------------------");
        log.info("--------------------------------------------");
    }

    public void onStart(ISuite suite) {
        //when suite tag starts
        System.out.println("STARTING TEST SUITE --> " + suite.getName());
        System.out.println("--------------------------------------------");
        log.info("STARTING TEST SUITE --> " + suite.getName());
        log.info("--------------------------------------------");
    }

    public void onFinish(ISuite suite) {
        //when suite tag completes
        System.out.println("TEST SUITE COMPLETED --> " + suite.getName());
        System.out.println("--------------------------------------------");
        log.info("TEST SUITE COMPLETED --> " + suite.getName());
        log.info("--------------------------------------------");

        emails = new SendEmail();
        try {
            emails.sendEmailWithAttachments();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }

    //TEST INITIALIZATION. This is necessary to add logs to report
    public void testInit(ITestResult result, String author, String category){
        xTest = xTestMan.startTest(result.getName());
        xTest.assignAuthor("Tested by: " + author );
        xTest.assignCategory(category);

        //FIND ME
        log.info("TEST INIT INSIDE LISTENER");
    }

}