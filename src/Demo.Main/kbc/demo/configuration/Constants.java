package kbc.demo.configuration;

import kbc.demo.base.BasePageObject;
import org.openqa.selenium.WebDriver;

public class Constants extends BasePageObject {
    /*
        Trick:
        psf + TAB generates  public static final.
        psfs  + TAB generates  public static final String.
        psfi  + TAB generates  public static final int.
    */
    public static final String GOOGLE_HOME_URL = "https://www.google.ie/";
    public static final String KBC_HOME_URL = "https://www.kbc.ie/";
    public static final String KBC_OUR_PRODUCTS_URL = "https://www.kbc.ie/our-products";
    public static final String KBC_MORTGAGES_URL = "https://www.kbc.ie/our-products/mortgages";
    public static final String USER_DIR = "user.dir";
    public static final String VIDEO_CAPTURES_FOLDER = "VideoEvidence";

    public Constants(WebDriver driver) {
        super(driver);
    }
}
