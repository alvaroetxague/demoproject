package testBase;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import kbc.demo.base.BrowserFactory;
import kbc.demo.pages.Home;
import kbc.demo.reporting.ExtentManager;
import kbc.demo.reporting.ExtentTestManager;
import kbc.demo.utilities.GenericMethods;
import kbc.demo.utilities.Screenshots;
import kbc.demo.utilities.VideoRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;

import static kbc.demo.configuration.Constants.GOOGLE_HOME_URL;

public class BaseTest {
    //WEBDRIVER
    protected WebDriver driver;

    //UTILITIES
    protected GenericMethods gm;

    protected ExtentManager xManager;
    protected ExtentTestManager xTestMan;
    protected ExtentTest xTest;
    protected ExtentReports xReport;
    protected static final Logger log = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());

    //TEST EVIDENCE
    protected VideoRecord videoRecord;

    //PAGES
    protected Home home;
    //BEFORE METHOD
    @Parameters({"browser"})
    @BeforeMethod()
//    public void testSetup(@Optional("firefox") String browser) throws Exception {
    public void testSetup(@Optional("firefox") String browser, Method method) throws Exception {
        //INITIALIZE THE BROWSER
        driver = BrowserFactory.getDriver(browser);

        //INITIALIZE OBJECTS INSTANCES
        gm = new GenericMethods(driver);
        home = new Home(driver);

        xManager = new ExtentManager();
        xTestMan = new ExtentTestManager();
        xReport = xManager.createReportInstance();

        //INITIALIZE VIDEO RECORDER AND START RECORDING
        videoRecord = new VideoRecord();
        videoRecord.startRecording(getClass().getSimpleName());

        //FIND ME
        log.info("BEFORE METHOD SETUP");

        gm.goToUrl(GOOGLE_HOME_URL);
    }

    //AFTER METHOD
    @AfterMethod()
    public void testTeardown(ITestResult result) throws Exception {
        xTest = xTestMan.getTest();
        //CAPTURE SCREENSHOT FOR FAILED TESTS AND ATTACH IT TO THE REPORT
        if (result.getStatus() == ITestResult.FAILURE) {
            String path = Screenshots.takeScreenshot(driver, result.getName());
            xTest.fail("See screenshot below for further details on this fail.").addScreenCaptureFromPath(path);
        }
        videoRecord.stopRecording();
        gm.closeBrowser();

        log.info("AFTER METHOD");
    }

    public void testInit(){
        xTest = xTestMan.getTest();
        //FIND ME
        log.info("I'm inside BaseTest getting an xTest instance");
    }
}