package testPages;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.BaseTest;

import java.io.IOException;

public class LearningTest extends BaseTest {

    @Test(priority = 1)
    public void verifyFailedTest() throws IOException {
        testInit();
        System.out.println("Failed this test on purpose");
        xTest.log(Status.INFO, "Inside Failed Test");
        xTest.log(Status.INFO, "FAILED this test on purpose", MediaEntityBuilder.createScreenCaptureFromPath("001.png").build());
        Assert.assertTrue(false);
    }

    @Test(priority = 2, dependsOnMethods={"verifyFailedTest"})
    public void verifySkippedTest(){
        testInit();
        Assert.assertTrue(true);
        System.out.println("Skipped this test on purpose");
        xTest.log(Status.INFO, "Inside Skipped Test");
        xTest.log(Status.INFO, "Skipped this test on purpose");
    }

    @Test(priority = 3)
    public void verifyPassedTest(){
        testInit();
        Assert.assertTrue(true);
        System.out.println("Passed Test");
        xTest.log(Status.INFO, "Inside Passed Test");
        xTest.log(Status.INFO, "Test Info");
    }
}
