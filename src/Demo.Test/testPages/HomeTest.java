package testPages;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.BaseTest;

public class HomeTest extends BaseTest {

    @Test
    public void verifyTextIsDisplayed(){
        gm.waitForVisibilityOf(home.getCheckOutMortgageText(), 10);
        Assert.assertTrue(home.getCheckOutMortgageText().isDisplayed());
        System.out.println("Mortgages text is displayed.");
    }

    @Test
    public void verifyPageTitle(){
        gm.waitForVisibilityOf(home.getCheckOutMortgageText(), 10);
        System.out.println("Home Page Title is: " + gm.getPageTitle());
    }

    @Test
    public void verifyFailedTest(){
        Assert.assertTrue(false);
        System.out.println("Failed this test on purpose");
    }

    @Test(dependsOnMethods={"verifyFailedTest"})
    public void verifySkippedTest(){
        Assert.assertTrue(true);
        System.out.println("Skipped this test on purpose");
    }
}
