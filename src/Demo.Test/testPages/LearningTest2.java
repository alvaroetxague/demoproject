package testPages;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.BaseTest;

import java.io.IOException;

public class LearningTest2 extends BaseTest {

    @Test(priority = 1)
    public void verifyTest1() throws IOException {
        testInit();
        xTest.log(Status.INFO, "verifyTest1");
        Assert.assertTrue(true);
    }

    @Test(priority = 2, dependsOnMethods={"verifyTest1"})
    public void verifyTest2(){
        testInit();
        xTest.log(Status.INFO, "verifyTest2");
        Assert.assertTrue(true);
    }

    @Test(priority = 3)
    public void verifyTest3(){
        testInit();
        xTest.log(Status.INFO, "verifyTest3");
        Assert.assertTrue(true);
    }
}
